module.exports = ( options ) ->

  @add 'role:api,path:update', ( msg, respond ) ->
    respond null

    params = msg.args

    @act 'role:transaction,action:serial', {
      __steps: [
        {
          __action: 'role:util,action:sanitize'
          __body: params
        }
        {
          __action: 'role:util,action:rename'
          __rename:
            body: 'rawMessage'
        }
        {
          __action: 'role:crypto,action:encode'
          in: 'body'
        }
        {
          __action: 'role:pubsub,action:pub'
          in: 'encodedMessage'
        }
      ]
    }, process.stdout

  @add 'init:api', ( msg, respond ) ->
    @act 'role:web',
      routes:
        prefix: '/update'
        pin: 'role:api,path:*'
        map:
          update:
            POST: true
            name: ''
    , respond
